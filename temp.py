#! /bin/python3

import glob
import time
import RPi.GPIO as GPIO


PIN_RED = 10
PIN_YELLOW = 9
PIN_GREEN = 11
PIN_BUZZER = 17

GPIO.setmode(GPIO.BCM)
GPIO.setup(PIN_RED, GPIO.OUT, initial=GPIO.LOW)
GPIO.setup(PIN_YELLOW, GPIO.OUT, initial=GPIO.LOW)
GPIO.setup(PIN_GREEN, GPIO.OUT, initial=GPIO.LOW)
GPIO.setup(PIN_BUZZER, GPIO.OUT, initial=GPIO.LOW)

treshold_low = 15
treshold_avg = 35
treshold_high = 50

# Buzz = GPIO.PWM(PIN_BUZZER, 440)
# Buzz.start(50) 

def stop():
    GPIO.output(PIN_RED, GPIO.LOW)
    GPIO.output(PIN_YELLOW, GPIO.LOW)
    GPIO.output(PIN_GREEN, GPIO.LOW)
    GPIO.output(PIN_BUZZER, GPIO.LOW)
    GPIO.cleanup()
    exit(1)


base_dir = '/sys/bus/w1/devices/'

try:
    device_folder = glob.glob(base_dir + '28*')[0]
except IndexError:
    print("No temprature sensore found, please chek the connection")
    stop()

device_file = device_folder + '/w1_slave'

def read_raw_temp_data():
    f = open(device_file, 'r')
    lines = f.readlines()
    f.close()
    return lines

def read_temp():
    lines = read_raw_temp_data()

    while lines[0].strip()[-3:] != 'YES':
        time.sleep(0.2)
        lines = read_raw_temp_data()

    equals_pos = lines[1].find('t=')

    if equals_pos != -1:
        return float(lines[1][equals_pos+2:])/1000.0

try:

    while True:
        try:
            temp = read_temp()
        except IndexError:
            print("IndexError")
            continue

        if temp >= treshold_low and temp <= treshold_avg:
            print ("normal")
            GPIO.output(PIN_RED, GPIO.HIGH)
            GPIO.output(PIN_YELLOW, GPIO.LOW)
            GPIO.output(PIN_GREEN, GPIO.HIGH)
        elif temp >=treshold_avg and temp <= treshold_high:
            print("elevated")
            GPIO.output(PIN_RED, GPIO.LOW)
            GPIO.output(PIN_YELLOW, GPIO.HIGH)
            GPIO.output(PIN_GREEN, GPIO.HIGH)
            # TODO: alert warning
        elif temp >= treshold_high and temp <= treshold_high+30:
            print("danger")
            GPIO.output(PIN_RED, GPIO.HIGH)
            GPIO.output(PIN_YELLOW, GPIO.HIGH)
            GPIO.output(PIN_GREEN, GPIO.HIGH)
            GPIO.output(PIN_BUZZER, GPIO.HIGH)
            # TODO: alert attention
        else:
            print("out of bounds")
            GPIO.output(PIN_RED, GPIO.HIGH)
            GPIO.output(PIN_YELLOW, GPIO.LOW)
            GPIO.output(PIN_GREEN, GPIO.LOW)
            # TODO: alert attention

        print(temp)
        time.sleep(1)

except KeyboardInterrupt:
    print("Goodby")
finally:
    stop()
