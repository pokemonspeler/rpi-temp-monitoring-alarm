# RPI Temp Monitoring Alarm

A Simple project to monitor the temprature and give an alert above a certain treshhold

## Getting started

Excpected components:
- [ ] 3 LEDs
  - [ ] 3 100 omh resitor
- [ ] [1 Wire DS18B20-compatible sensor](https://www.adafruit.com/product/381)
  - [ ] 4.7k omf resistor
- [ ] passive buzzer

## information sources
- [temprature sensore configuration](https://learn.adafruit.com/adafruits-raspberry-pi-lesson-11-ds18b20-temperature-sensing/software)
- [usage of passive buzzer](https://www.circuitbasics.com/how-to-use-buzzers-with-raspberry-pi/)
- [rpi zero pinout](https://i.stack.imgur.com/yHddo.png)
